import Style from "../components/Line-Up"
import React from "react";

class Ulist extends React.Component {
    render() {
        return <div className={Style.body}>
            <div className={Style.card}>
                <p>Install React</p>
                <div className={Style.visual}>
                <ul>
                    <li>Using NPM</li>
                    <li>Using NPX</li>
                    <li>Using Yarn</li>
                </ul>
                </div>
            </div>
            </div>
    }
}

export default Ulist;