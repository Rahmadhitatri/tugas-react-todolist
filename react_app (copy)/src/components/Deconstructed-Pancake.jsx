import Style from "../assets/Deconstructed-Pancake.module.css";
import React from "react";

class Induk extends React.Component {
    render() {
        return <div className={Style.body}>
            <br></br>
            <div className={Style.parent}>
                <div className={Style.child}>1</div>
                <div className={Style.child}>2</div>
                <div className={Style.child}>3</div>
            </div>
        </div>
    }
}

export default Induk;